(ns ampere-db-utils.export
  (:require [ampere-db-utils.core :as c]
            [ampere-db-utils.model :as m]
            [clojure.java.io :as io]
            [taoensso.timbre :as timbre :refer [log debug info warn error spy]]
            [clojure.string :as string]))

(defn export-table!
  "Export filtered data as json from table to file"
  [db dm table where file]
  (let [json (:json (m/query-json db dm table where))]
    (io/make-parents file)
    (spit file json)
    table))

(defn export-tables!
  "Export sequence of tables as json files to directory, with filter
  generated for table by (where-fn db dm table)"
  [db dm tables where-fn dir]
  (doall (for [table tables]
           (do (debug "Exporting " table)
               (export-table! db dm table
                              (apply where-fn [db dm table])
                              (str dir "/" (name table) ".json"))))))

(defn export-entities!
  "Export entity types for tables to directory"
  [db dm tables entities dir]
  (doall (for [entity entities]
           (do (debug "Exporting Entity:" entity)
               (export-tables! db dm tables
                               (constantly ["ad_client_id=0 and entitytype = ?" entity])
                               (str dir "/" entity))))))

(defn export-client!
  "Export entity types for tables to directory"
  [db dm tables client dir]
  (debug "Client:" client)
  (export-tables! db dm tables
                  (constantly ["ad_client_id = ?" client])
                  dir))

;; Utilities to dump table data out to postgresql text file (using "COPY" command)
;; Quicker to export and load but assumes consistent db schema
(c/load-sql "export.sql")

(defn dump-table!
  "Export a table to postgresql text file (using \"COPY\" syntax)"
  [db dm tk where dir]
  (let [ 
        cols (m/cols dm tk)
        tmpfile (str "/tmp/" (name tk) ".sql")
        filename (str "'" tmpfile "'")
        outfile (str dir "/" (name tk) ".sql")
        params {:filename filename
                :columnnames cols
                :tablename (name tk)
                :wc where}]
    (try
      (if (> (:count (count-table db {:tablename (name tk)
                                      :wc where})) 0)
        (do
          (clojure.java.io/make-parents outfile)
          (spit outfile (first (copy-table-from-stdin-sqlvec params)))
          (copy-table-to db params)
          (spit outfile (str "\n" (slurp tmpfile) "\\.\n") :append true)
          (info "Exported" tk "to" outfile))
        (info (str "No rows in " (name tk))))
      (catch Exception e (error "Error exporting " (name tk)
                                ": " (.getMessage e))))))

(defn dump-tables!
  "Export tables filtered by where-fn to postgresql text file."
  [db dm tables where-fn dir]
  (doseq [table tables]
           (dump-table! db dm table
                                  (apply where-fn [db dm table])
                                  dir)))

(defn dump-clients! [db dm clients tables dir]
  (doseq [client clients]
    (debug "Exporting client" client)
    (dump-tables! db dm tables
                            (constantly [(str "ad_client_id=" (:ad_client_id client))])
                            (str dir "/" (:name client) "/Base/"))))

(defn dump-entities! [db dm clients entities tables dir]
  (doseq [client clients]
    (debug "Exporting client" client)
    (doseq [entity entities]
      (debug "Exporting entity" entity)
      (dump-tables! db dm tables
                              (constantly [(str "ad_client_id=" (:ad_client_id client)
                                                " and entitytype='" entity "'")])
                              (str dir "/" (:name client) "/" entity)))))

(defn entity-list
  "Get a list of Entity names from the database"
  [db]
  (map :entitytype (get-entitytypes db)))

(defn export-table-list
  "Get list of tables without EntityType columns"
  [db]
  (map #(-> % :tablename string/lower-case keyword) (get-tables-to-export db)))

(defn entity-table-list
  "Get a list of tables with EntityType columns"
  [db]
  (map #(-> % :tablename string/lower-case keyword) (get-entity-tables db)))

(defn dump-system
  "Export entity tables as postgresql text files to {dir}/{clientname}/{entityname}/"
  [db dir]
  (let [edm (m/load-db-data-model db)
        clients (get-clients db)
        entities (entity-list db)
        etables (entity-table-list db)]
    (dump-entities! db edm clients entities etables dir)))

(defn dump-clients
  "Export non-entity tables as postgresql text files to {dir}/{clientname}/Base/"
  [db dir]
  (dump-clients! db
                           (m/load-db-data-model db)
                           (get-clients db)
                           (export-table-list db)
                           dir))

(ns ampere-db-utils.ampere-system
  (:require [ampere-db-utils.core :as c]
            [ampere-db-utils.export :as e]
            [ampere-db-utils.import :as i]
            [ampere-db-utils.model :as m]
            [ampere-db-utils.sync :as sync]
            [cheshire.core :refer :all]
            [clojure.java.jdbc :as jdbc]
            [clojure.set :as st]
            [hikari-cp.core :as cp ]))


(def entity-tables #{:ad_table :ad_window :ad_element :ad_process})

(def child-tables #{:ad_column
                   :ad_ref_list
                   :ad_ref_table
                   :ad_process_para
                   :ad_tab
                   :ad_field
                   :ad_impformat_row
                   :ad_view_column
                   :ad_view_definition
                   :ad_importcolumn
                   :ad_importvalidation
                   :ad_chartdatasource
                   :ad_printformatitem
                   :ad_scheduler_para
                   :ad_columnprocess_para
                   :ad_reportcolumnset
                   :ad_browse_field
                   :ad_wf_node
                   :ad_wf_nodenext
                   :ad_infocolumn})

(defn- get-entity-types
  []
  (map #(:entitytype %)
                         (jdbc/query c/db ["SELECT entitytype from ad_entitytype
                                  order by 1"])))

(defn- get-tables
  []
  (jdbc/query c/db ["SELECT lower(tablename) as tablename from ad_table
                                 WHERE isactive = 'Y'
                                 AND isview = 'N'
                                 AND tablename not like e'T\\_%'
                                 AND tablename not like e'I\\_%'
                                 AND tablename not like e'U\\_%'
                                 AND tablename not in ('AD_Attribute_Value', 'AD_PInstance_Log', 'AD_PInstance', 'AD_RecentItem', 'AD_Session')
                                 AND EXISTS (SELECT * FROM AD_column c where
                                               c.columnname = 'AD_Client_ID'
                                               AND c.ad_table_id=ad_table.ad_table_id)
                                  order by 1"]))

(defn- counted-tables
  [tablelist]
  (map #(merge % (first 
                             (jdbc/query c/db [(str "SELECT count(*) from "
                                                   (:tablename %)
                                                   " where ad_client_id = 0")])))
                  tablelist))

(defn make-table-list 
  []
  (let [tablelist (get-tables)
        tbct (counted-tables tablelist)
        tables-with (doall (filter #(> (:count %) 0) tbct))]
    (into #{} (map #(keyword (:tablename %)) tables-with))))

(defn make-data-model
  []
  (let [edm (m/load-db-data-model c/db)]
        ;; define some parent/child relations
          (-> edm
              (update :ad_table assoc :children [{:tablename "ad_column"}])
              (update :ad_reference assoc :children [{:tablename "ad_ref_list"}
                                                     {:tablename "ad_ref_table"}])
              (update :ad_process assoc :children [{:tablename "ad_process_para"}])
              (update :ad_window assoc :children [{:tablename "ad_tab"}])
              (update :ad_tab assoc :children [{:tablename "ad_field"}])
              (update :ad_impformat assoc :children [{:tablename "ad_impformat_row"}])
              (update :ad_view assoc :children [{:tablename "ad_view_column"}
                                                {:tablename "ad_view_definition"}])
              (update :ad_import assoc :children [{:tablename "ad_importcolumn"}
                                                  {:tablename "ad_importvalidation"}])
              (update :ad_chart assoc :children [{:tablename "ad_chartdatasource"}])
              (update :ad_printformat assoc
                      :children [{:tablename "ad_printformatitem"}])
              (update :ad_scheduler assoc
                      :children [{:tablename "ad_scheduler_para"}])
              (update :ad_columnprocess assoc
                      :children [{:tablename "ad_columnprocesspara"}])
              (update :ad_reportcolumn assoc
                      :children [{:tablename "ad_reportcolumnset"}])
              (update :ad_browse assoc :children [{:tablename "ad_browse_field"}])
              (update :ad_workflow assoc :children [{:tablename "ad_wf_node"}])
              (update :ad_wf_node assoc :children [{:tablename "ad_wf_nodenext"}])
              (update :ad_infowindow assoc :children [{:tablename "ad_infocolumn"}]))))

;; remove the child tables and entity tables from the export list
(defn base-tables
  []
  (st/difference (make-table-list) child-tables entity-tables))

(defn export-system
  [dir]
  (let [dm (make-data-model)]
    (clojure.java.io/make-parents (str dir "/data-model.edn"))
    (m/write-data-model dm (str dir "/data-model.edn"))
    (e/export-client! c/db dm (base-tables) 0 (str dir "/System/Base") )
    (e/export-entities! c/db dm entity-tables (get-entity-types) (str dir "/System"))))

(defn dump-base
  [dir]
  (e/dump-clients c/db dir))

(defn dump-entities
  [dir]
  (e/dump-system c/db dir))

(defn dump-all
  [dir]
  (doall
   (e/dump-clients c/db dir)
   (e/dump-system c/db dir)))

(def datasource-options {:auto-commit        true
                         :read-only          false
                         :connection-timeout 30000
                         :validation-timeout 5000
                         :idle-timeout       600000
                         :max-lifetime       1800000
                         :minimum-idle       10
                         :maximum-pool-size  10
                         :pool-name          "db-pool"
                         :jdbc-url (:connection-uri c/db)
                         :register-mbeans    false})

(def datasource
  (delay (cp/make-datasource datasource-options)))


(defn load-records
  "Load json data from directory"
  [dir]
  (i/flatten-children (i/load-dir (str dir "/System")) []))

(defn load-dm
  "Load data model from directory"
  [dir]
  (m/load-file-data-model (str dir "/data-model.edn")))

(defn sync-system
  "Sync System client from files in directory to database"
  [dir]
  (let [records (load-records dir)
        dm (load-dm dir)]
    (jdbc/with-db-connection [conn {:datasource @datasource}]
      (sync/run-sync-repeatedly conn dm records))))

(comment

  


  (defn mand-data
    [table data]
    (select-keys (first data) (m/mand-cols table)))

  (defn get-mand-data
    [table id]
    (mand-data table
               (jdbc/query c/db [(str "select * from "
                                     (name table)
                                     " where "
                                     (name table) "_id = ?") id])))

  (def user-100-mand (get-mand-data :ad_user 100))
;;;;;;;;;;
  (def user-0-mand (get-mand-data :ad_user 0))
;;;;;;;;;;
  (def org-0-mand (get-mand-data :ad_org 0))
;;;;;;;;;;
  (def client-0-mand (get-mand-data :ad_client 0))
  
  (jdbc/insert! {:datasource @datasource}
                :ad_client client-0-mand)
  (jdbc/insert! {:datasource @datasource}
                :ad_org org-0-mand)
  (jdbc/insert! {:datasource @datasource}
                :ad_user user-0-mand)
  (jdbc/insert! {:datasource @datasource}
                :ad_user user-100-mand)
  )

;; execute sync run
(comment
  (sync/clear-cache)

  (def run1
    (jdbc/with-db-connection [conn {:datasource @datasource}]
             (sync/run-sync-repeatedly conn adm records)))
  
  (cp/close-datasource @datasource)
  )

;; analyse errors from sync
(comment 
  (def grouped (group-by :tablename (sync/error-summary run1)))

  (reduce-kv #(conj %1 (vector %2 (count %3))) [] grouped)

  (def error-by-table (reduce-kv #(conj %1 (vector (keyword %2) %3 )) {} grouped))

  )

;; drop tables not in AD
(comment 
  (def db-tables
    (into #{} (map (comp keyword :tablename) (jdbc/query c/db ["SELECT * from pg_tables where schemaname = 'ampere'"]))))

  (def dm-tables
    (into #{} (keys edm)))

  (def excess-tables (st/difference db-tables dm-tables))

  (defn drop-table
    [db t]
    (let [ddl (jdbc/drop-table-ddl t )]
      (jdbc/execute! db ddl)))

  (defn drop-tables
    [db ts]
    (map #(drop-table % db) ts))
  )

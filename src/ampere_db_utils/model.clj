(ns ampere-db-utils.model
  (:require [ampere-db-utils.core :as c]
            [cheshire.core :as json]
            [cheshire.parse :as parse]
            [clojure.string :as str]
            [clojure.walk :as cw]
            [fipp.edn :as fp]
            [java-time :as t]
            [taoensso.timbre :as timbre :refer [debug error info log spy warn]])
  (:import org.apache.commons.codec.binary.Hex))

(alter-var-root #'parse/*use-bigdecimals?* (constantly true))

(declare child-snips)

(defn convert-table-info
  "Convert json table info query into map"
  [ti]
  (-> ti
      :info
      .getValue
      (json/parse-string true)
      (update :tablename keyword)
      (update :key_column keyword)
      (update :identifiers #(into [] (map keyword %)))
      (update :columns #(into []
                              (map (fn [m]
                                     (-> m
                                         (update :fk_table keyword)
                                         (update :fk_column keyword))) %)))))

(defn load-db-data-model
  "Load Application Dictionary data model from db"
  [db]
  (into {} (map #(let [ti (convert-table-info %)]
                   [(:tablename ti) ti]) (c/get-table-info db))))

(defn remove-nils [m]
  (let [f (fn [x]
            (if (map? x)
              (let [kvs (filter (comp not nil? second) x)]
                (if (empty? kvs) nil (into {} kvs)))
              x))]
    (clojure.walk/postwalk f m)))

(defn write-data-model
  "Write data model to file f"
  [dm f]
  (spit f (with-out-str (fp/pprint (into (sorted-map) (remove-nils dm))))))

(defn load-file-data-model
  "Load Application Dictionary data model from file"
  [f]
  (read-string (slurp f)))

(defn cols
  "Returns vector of column names for table name
  from data model"
  [dm tn]
  (let [columns (get-in dm [tn :columns])]
    (into [] (map #(-> %
                       :columnname
                       str/lower-case) columns))))

(defn sort-cols
  "Returns vector of column names for table name
  from data model"
  [dm tn]
  (let [key-column (get-in dm [tn :key_column])
        parents (get-in dm [tn :parent_columns])]
    (cond
      key-column [(name key-column)]
      parents (map name parents)
      :else (cols dm tn))))

(defn add-key
  "Add :key map to record with data model dm if missing
   e.g. {:tablename \"ad_window\"
         :key {:ad_window_id 100}}"
  [dm rec]
  (if (:key rec)
    rec
    (let [tn (keyword (:tablename rec))
          m (:data rec)
          key-column (get-in dm [tn :key_column])
          parents (get-in dm [tn :parent_columns])]
      (assoc rec 
             :key (if key-column
                    {key-column (or (get m key-column) (get m (keyword key-column)))}
                    (into {} (map #(vector % (or (get m %)
                                                 (get m (keyword %)))) parents)))))))

(defn add-identifier
  "Add human readable :identifier map to record with data model dm,
  if missing"
    [dm rec]
  (if (:identifier rec)
    rec
    (let [tn (keyword (:tablename rec))
          ids (get-in dm [tn :identifiers])]
      (assoc rec
             :identifier
             (select-keys (:data rec) ids)))))

(defn split-by-keys
  "Divide map into two map vector [included excluded] on list of keys to include"
  [m ks]
  [(select-keys m ks) (apply dissoc m ks)])

(defn- child-snip
  "Create query snippet for retrieving child records
  given parent table key, child {:tablename \"child_table\"
                                 :fk_column \"fk_id\"} .
  Assumes parent primary key is used"
  [dm parent child]
  (let [pt (name parent )
        pk (name (get-in dm [parent :key_column]))
        ct (:tablename child)
        ck (:fk_column child)
        fk (or ck pk)
        wc (str pt "." pk " = " ct "." fk)
        cs (child-snips dm (keyword ct))]
    (c/child-json-snip {:tablename ct
                      :columnnames (cols dm (keyword ct))
                      :sortnames (sort-cols dm (keyword ct))
                      :wc wc
                      :children cs})))

(defn child-snips
  "Get vector of SQL snippets for querying child records of a table.
  Requires input map in containing :children key with list of
  {:tablename t :fk-column fk}.  Returns [[]] if children missing
  to satisfy hugsql query.  "
  [dm table]
  (let [tm (table dm)
        parent (:tablename tm)
        children (:children tm)] 
    (into [] (if (seq children)
               (for [child children]
                 (child-snip dm parent child))
               [[]]))))

(defn query-json
  "Query db using data model dm to extract table data as json,
  filtered by the where snippet"
  [db dm table where] 
  (let [params {:tablename (name table)
                :columnnames (cols dm table) 
                :sortnames (sort-cols dm table)
                :children  (child-snips dm table)
                :wc where}]
    (c/select-json db params)))

(defn query-record
  "Retrieve a single record from the database"
  [db dm table where]
  (let [params {:tablename (name table)
                :columnnames (cols dm table) 
                :sortnames (sort-cols dm table)
                :children [[]]
                :wc where}]
    (some-> (c/select-json db params)
            :json
            .getValue
            (json/parse-string true)
            first)))

(def ampere-data-types
  "Ampere reference data types"
  {10            {:name "String", :datatype :string},
   11            {:name "Integer", :datatype :number},
   12            {:name "Amount", :datatype  :number},
   13            {:name "ID", :datatype :number},
   15            {:name "Date", :datatype :datetime},
   16            {:name "Date+Time", :datatype :datetime},
   17            {:name "List", :datatype :string},
   18            {:name "Table", :datatype :table},
   19            {:name "Table Direct", :datatype :table},
   20            {:name "Yes-No", :datatype :boolean},
   21            {:name "Location (Address)", :datatype :table},
   22            {:name "Number", :datatype :number},
   23            {:name "Binary", :datatype :binary},
   24            {:name "Time", :datatype :datetime},
   25            {:name "Account", :datatype :table},
   26            {:name "RowID", :datatype :string},
   27            {:name "Color", :datatype :table},
   28            {:name "Button", :datatype :string},
   29            {:name "Quantity", :datatype :number},
   30            {:name "Search", :datatype :table},
   31            {:name "Locator (WH)", :datatype :table},
   32            {:name "Image", :datatype :table},
   33            {:name "Assignment", :datatype :table},
   34            {:name "Memo", :datatype :string},
   35            {:name "Product Attribute", :datatype :table},
   37            {:name "Costs+Prices", :datatype :number},
   53370         {:name "Chart", :datatype :table}})

(defmulti coerce-json-value (fn [t _] t))
(defmethod coerce-json-value :datetime [_ val]
  (if (instance? java.time.LocalDateTime val)
    {:value val}
    (try {:value (when val (t/local-date-time val))}
         (catch Exception _
           (let [msg "Failed to convert to datetime"]
             (debug msg val)
             {:error msg :err-val val})))))
(defmethod coerce-json-value :binary [_ val]
  (if (bytes? val)
    {:value val}
    (try
      (if (.startsWith val "\\x")
        {:value 
         (-> (subs val 2)
             .toCharArray
             Hex/decodeHex)}
        {:error "Unknown binary encoding" :err-val val})
      (catch Exception e
        (let [msg "Failed to extract binary data"]
          (debug msg val (.getMessage e))
          {:error msg :err-val val})))))
(defmethod coerce-json-value :default [_ val]
  {:value val})

(defn col-info
  "Get table column info from datamodel"
  [dm t c]
  (first (filter #(= c (keyword (:columnname %))) (:columns (t dm)))))

(defn datatype
  "Get datatype for provided column info"
  [col]
  (:datatype (get ampere-data-types (:ad_reference_id col))))

(defn coerce-column-value
  "Coerce value v of column k from table info ti into appropriate type for updating db,
  returns [coerced-value error]"
  [dm t c v]
  (when (not (nil? v))
    (let [col (col-info dm t c)
          type (datatype col)
          coerced (coerce-json-value type v)]
      (if-let [n (:value coerced)]
        [n nil]
        [nil coerced]))))

(defn coerce-json-data
  "Take record and update :data map with values coerced to appropriate type for
  data model,
  removing any invalid keys"
  [dm rec]
  (let [t (-> rec
              :tablename
              keyword)
        d (:data rec)
        [inc exc] (split-by-keys d (map keyword (cols dm t)))
        {:keys [coerced errors warnings]} (reduce-kv (fn [r k v]
                        (let [[c e] (coerce-column-value dm t k v)]
                          (if e
                            (-> r
                                (assoc-in [:warnings k] e)
                                (assoc-in [:errors k] v))
                            (assoc-in r [:coerced k] c))))
                        {:coerced {} :errors {} :warnings {}} inc)]
    (cond-> rec
        true (assoc :data coerced)
        (seq exc) (assoc-in [:warnings :excluded] exc)
        (seq warnings) (assoc-in [:errors :coerce-errors] warnings)
        (seq errors) (assoc-in [:errors :coerce] errors))))

(defn mandatory-cols
  "Return list of mandatory columns for table"
  [dm t]
  (into [] (map #(-> %
                     :columnname
                     keyword)
                (filter #(or (= "Y" (:ismandatory %))
                             (= "Y" (:iskey %)))
                        (get-in dm [t :columns])))))

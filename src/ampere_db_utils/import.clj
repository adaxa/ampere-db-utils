(ns ampere-db-utils.import
  (:require [ampere-db-utils.model :as m]
            [cheshire.core :as json]
            [cheshire.parse :as parse]
            [clojure.java.io :as io]
            [clojure.java.jdbc :as jdbc]
            [taoensso.timbre :as timbre :refer [log debug info warn error spy]])
  (:import java.sql.SQLException))

(alter-var-root #'parse/*use-bigdecimals?* (constantly true))

(defn load-files
  "Load a list of files containing json array into list"
  [files]
  (reduce #(concat %1 (json/parse-stream (io/reader %2) true)) [] files))

(defn- files-in-dir
  [dir]
  (-> str
      (mapv (filter #(.isFile %) (file-seq (clojure.java.io/file dir))))
      sort))

(defn load-dir
  "Load all files in directory"
  [dir]
  (let [files (files-in-dir dir)]
    (debug "Loading files" files)
    (load-files files)))

(defn flatten-children
  "Take list of nested maps containing :children key and recursively flatten into result"
  [s result]
  (reduce (fn [out m]
            (if-let [children (seq (:children m))]
              (flatten-children children
                                (conj out (dissoc m :children)))
              (conj out (dissoc m :children))))
          result s))

(defn insert-record!
  "Try inserting row in db and update out map with results/errors"
  [db out r]
  (let [t (:tablename r)
        m (:data r)
        result (try
                  {:ok (jdbc/insert! db t m)}
                  (catch SQLException e {:err {:record r
                                               :sqlstate (.getSQLState e)
                                               :msg (.getMessage e)}})
                  (catch Exception e {:err {:msg (.getMessage e)
                                            :record r}}))]
     (if (:ok result)
       (do 
         (debug "Inserted " t (:ok result))
         (update out :inserts #(if %
                                 (inc %)
                                 1)))
       (do
         (debug "Insert failed " t (get-in result [:err :msg]))
         (update out :insert-errors #(if (seq %1)
                                       (conj %1 %2)
                                       [%2]) result)))))

(defn import-files
  "Load data from list of files and insert into db without validation other than
  coercing data into appropriate types as specified by data model"
  [db dm files]
  (let [rs (-> files
               load-files
               (flatten-children []))]
    (reduce #(insert-record! db %1 (m/coerce-json-data dm %2)) {} rs)))

;; (defn import
;;   "Import a single json record into target db"
;;   [db dm record]
  
;;   )








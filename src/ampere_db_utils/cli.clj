(ns ampere-db-utils.cli
  (:gen-class)
  (:require [ampere-db-utils.ampere-system :as as]
            [cli-matic.core :as cli]
            ))

(defn sync-system
  "Synchronise System client with json data from directory"
  [{:keys [dir]}]
  (as/sync-system dir)
  0)

(defn export-system
  "Export System client as json to specified directory"
  [{:keys [dir]}]
  (as/export-system dir)
  0)

(defn dump-base
  "Export base tables as sql \"COPY\" for all clients to directory"
  [{:keys [dir]}]
  (as/dump-base dir)
  0)

(defn dump-entities
  "Export base tables as sql \"COPY\" for all clients to directory"
  [{:keys [dir]}]
  (as/dump-entities dir)
  0)

(defn dump-all
  "Export all tables as sql \"COPY\" for all clients to directory"
  [{:keys [dir]}]
  (as/dump-all dir)
  0)

;; cli-matic configuration
(def cli-conf
  {:command     "ampere-db-utils"
   :description "A command-line utility for managing ampere database"
   :version     "0.0.1"
   :subcommands [{:command     "export"
                  :description "Export Ampere data to json files"
                  :subcommands [{:command     "system"
                                 :description "Export System client (Application Dictionary) to json files"
                                 :examples    ["export system --dir db"]
                                 :opts        [{:as     "Output directory"
                                                :option "dir"
                                                :default "system"
                                                :type   :string}
                                               ]
                                 :runs        export-system}]
                  }
                 {:command     "dump"
                    :description "Export Ampere data to sql files"
                  :subcommands [{:command     "all"
                                 :description "Export all tables for all clients"
                                 :examples    ["export system --dir db"]
                                 :opts        [{:as     "Output directory"
                                                :option "dir"
                                                :default "data"
                                                :type   :string}
                                               ]
                                 :runs        dump-all}
                                {:command     "base"
                                   :description "Export base tables for all clients"
                                   :examples    ["export system --dir db"]
                                   :opts        [{:as     "Output directory"
                                                  :option "dir"
                                                  :default "data"
                                                  :type   :string}
                                                 ]
                                   :runs        dump-base}
                                  {:command     "entities"
                                   :description "Export entity tables for all clients"
                                   :examples    ["export system --dir db"]
                                   :opts        [{:as     "Output directory"
                                                  :option "dir"
                                                  :default "data"
                                                  :type   :string}
                                                 ]
                                   :runs        dump-entities}]
                    }
                 {:command     "sync"
                  :description "Sync data from json files to database"
                  :subcommands [{:command     "system"
                                 :description "Synchronise System client (Application Dictionary) with json data"
                                 :examples    ["import system --dir db"]
                                 :opts        [{:as     "Source directory"
                                                :option "dir"
                                                :default "system"
                                                :type   :string}
                                               ]
                                 :runs        sync-system}]
                  }]} )

(defn -main [& args]
  (cli/run-cmd args cli-conf))

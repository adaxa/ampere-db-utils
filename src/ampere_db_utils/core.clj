(ns ampere-db-utils.core
  (:require [aero.core :refer [read-config]]
            [clojure.java.io :as io]
            [hugsql.core :as hugs]))

(def config (read-config (io/resource "config.edn")))

(def db (:db config))

(defn create-db
  "Create postgres database config for given params"
  [host port name user pw]
  {:connection-uri (str "jdbc:postgresql://" host ":" port "/" name
                        "?user=" user "&password=" pw)})

(defn load-sql
  "Load hugsql functions from provided resource"
  [filename]
  (doall
   (hugs/def-db-fns filename {:quoting :ansi})
   (hugs/def-sqlvec-fns filename {:quoting :ansi})))

(load-sql "ampere-db-utils.sql")



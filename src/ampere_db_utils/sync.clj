(ns ampere-db-utils.sync
  (:require [ampere-db-utils.core :as c]
            [ampere-db-utils.import :as i]
            [cheshire.core :as json]
            [cheshire.parse :as parse]
            [clojure.data :as d]
            [clojure.java.io :as io]
            [clojure.java.jdbc :as jdbc]
            [clojure.string :as str]
            [hugsql.core :as hugs]
            [java-time :as t]
            [taoensso.timbre :as timbre :refer [debug error info log spy warn]]
            [taoensso.timbre.appenders.core :as la]
            [ampere-db-utils.model :as m])
  (:import java.sql.SQLException
           org.apache.commons.codec.binary.Hex))

(declare placeholder-value sync-record)

(def record-lookup (atom {}))
(def currently-syncing (atom {}))
(def ^:dynamic *dm*)

(timbre/merge-config! {:appenders
                       {:println {:enabled? false}
                        :spit
                        (la/spit-appender {:fname "log/sync.log"})}})

(defn get-record
  "Retrieve from target database table t row corresponding to record"
  [db rec]
    (some-> db
            (c/select-json-where (assoc rec :wc (c/where-key-snip rec)))
            :json
            .getValue
            (json/parse-string true)))

(defn create-placeholder-record
  "Create a placeholder record in table"
  [db t]
  (let [cols (map #(-> % :columnname str/lower-case)
                  (filter #(= "Y" (:ismandatory %)) (get-in *dm* [t :columns])))
        key-column (get-in *dm* [t :key_column])
        _ (debug "Creating placeholder for " t)
        in (into {} (map #(vector % (placeholder-value db t %)) cols))
        placeholder (assoc in (name key-column) -1)]
    (jdbc/insert! db (name t) placeholder)))

(defn check-placeholder
  "Check placeholder record for table t (creating if required).
  Returns -1 if placeholder exists"
  [db t]
  (let [key-column (get-in *dm* [t :key_column])]
    (when (or (get-record db {:tablename (name t)
                              :key {key-column -1}})
            (create-placeholder-record db t))
      -1)))

(defn placeholder-value
  "Create a placeholder value for missing data"
  [db t c]
  (let [col (m/col-info *dm* t (keyword c))
        m? (= "Y" (:ismandatory col))
        type (m/datatype col)]
    (when true
      (condp = (name c)
        "ad_client_id" 0
        "ad_org_id"    0
        "ad_user_id"   0
        "createdby"    0
        "updatedby"    0
        "entitytype"   "D"
        (condp = type
          :number 0
          :boolean "N"
          :datetime (t/local-date-time)
          :table (if-let [ft (:fk_table col)]
                   (check-placeholder db ft)
                   nil)
          "")))))

(def validated-fks (atom #{}))

(defn clear-cache
  []
  (reset! validated-fks #{})
  (reset! record-lookup {})
  (reset! currently-syncing {}))

(defn- create-missing-fk
  [rec db v t c]
  (if-let [r (get-in @record-lookup [(:tablename rec)
                                     (:key rec)])]
    (if-not (get-in @currently-syncing [(:tablename rec)
                                          (:key rec)])
      (let [_(sync-record db r)]
        ;; check for error here
        [v nil])
      [(placeholder-value db t c) v])
    [(placeholder-value db t c) v]))

(defn check-fk-value
  "Check if fk reference exists in database,
  returns [valid/substitute nil/invalid]"
  [db t c v]
  (let [col (m/col-info *dm* t c)]
    (if (nil? (:fk_table col))
      [nil v]
      (let [ rec {:tablename (name (:fk_table col))
                  :key {(:fk_column col) v}}
            exists? (or (contains? @validated-fks rec)
                        (contains? (when (seq (get-record db rec))
                                     (swap! validated-fks #(conj % rec)))
                                   rec))]
        (if exists?
          [v nil]
          (create-missing-fk rec db v t c))))))

(defn check-foreign-keys
  "Check foreign key references in record exist in target database, substitute valid
  placeholder if required."
  [db rec]
  (let [t (-> rec :tablename keyword)
        d (:data rec)
        {:keys [valid subs]}
        (reduce-kv
         (fn [r c v]
           (let [col (m/col-info *dm* t c)
                 type (m/datatype col)]
             (if (or (nil? v)
                     (not (= :table type))
                     (= c (get-in *dm* [t :key_column])))
               (assoc-in r [:valid c] v)
               (let [[vc e] (check-fk-value db t c v)]
                 (cond-> r
                   (not (nil? e)) (assoc-in [:subs c] e)
                   true (assoc-in [:valid c] vc))))))
           {:valid {} :subs {}} d)]
    (cond-> rec
      true (assoc :data valid)
      (seq subs) (assoc-in [:errors :substituted] subs))))

(defn- fill-mandatory [db rec]
  (let [t (-> rec :tablename keyword)
        d (:data rec)
        cols (filter #(not (% d)) (m/mandatory-cols *dm* t))]
    (assoc rec :data
           (reduce #(assoc %1 %2 (placeholder-value db t (name %2)) ) d cols))))

(defn prep-record
  "Validate record against data model, coercing strings to objects as required,
  adding key and identifiers"
  [db rec]
  (if (not (and (contains? rec :tablename)
                (contains? rec :data)))
    (assoc rec :errors :invalid-record)
    (if (not (get *dm* (keyword (:tablename rec))))
      (assoc rec :errors :invalid-table)
      (-> (->> rec
               (m/coerce-json-data *dm*)
               (m/add-key *dm*)
               (m/add-identifier *dm*)
               (fill-mandatory db))
          (assoc :prepped? true)))))

(defn prepare-data
  "Validate record against data model by checking foreign keys"
  [db rec]
  (if (not (and (contains? rec :tablename)
                (contains? rec :data)))
      (assoc rec :errors :invalid-record)
      (if (not (get *dm* (keyword (:tablename rec))))
        (assoc rec :errors :invalid-table)
        (-> (->> rec
                (check-foreign-keys db))
             (assoc :validated? true)))))

(defn- do-update
  [db t c where]
  (try
    {:ok (first (jdbc/update! db t (:data c) where
                              {:entities (jdbc/quoted :ansi)}))}
    (catch SQLException e {:error {:sqlstate (.getSQLState e)
                                   :msg (.getMessage e)}})
    (catch Exception e {:error {:msg (.getMessage e) }})))

(defn update-record
  [db rec target]
  (let [
        oldv (prepare-data db target)
        diff (d/diff (:data oldv) (:data rec))
        diff? (or (first diff) (second diff))
        t (:tablename rec)
        c (prepare-data db rec)]
    (if diff?
      (let [where (c/where-key-snip rec)
            _ (debug "Diff from: " (first diff) " to: " (second diff))
            result (do-update db t c where)]
          (if (:ok result)
            (do 
              (debug "Updated " t (:ok result))
              (assoc c :sync :success))
            (do
              (debug "Update failed " t (get-in result [:error :msg]))
              (-> c
                  (assoc :sync :error)
                  (assoc-in [:errors :update] result)))))
      (assoc c :sync :success :sync-action :no-change))))

(defn insert-record
  "Insert record into db, returning the modified record"
  [db rec]
  (let [t (:tablename rec)
        c (prepare-data db rec)
        result (try
                 {:ok (jdbc/insert! db t (:data c)
                                    {:entities (jdbc/quoted :ansi)})}
                 (catch SQLException e {:error {:sqlstate (.getSQLState e)
                                              :msg (.getMessage e)}})
                 (catch Exception e {:error {:msg (.getMessage e) }}))]
    (if (:ok result)
      (do
        (debug "Inserted " t (:ok result))
        (assoc c :sync :success))
      (do
        (debug "Insert failed " t (get-in result [:error :msg]))
        (-> c
            (assoc :sync :error)
            (assoc-in [:errors :insert] result))))))

(defn sync-record
  "Take a record and sync to the target db using provided data model, returning record
  with result or errors inserted"
  [db rec]
  (try
    (swap! currently-syncing assoc-in [(:tablename rec)
                                       (:key rec)] true)
    (let [vc (prepare-data db rec)
          result (if (:validated? vc)
                   (let [where (c/where-key-snip vc)
                         tm (m/query-record db *dm* (keyword (:tablename rec)) where)
                         _ (debug "Sync " (select-keys vc [:tablename :key :identifier]))]
                     (if tm
                       (update-record db (assoc vc :sync-action :update) tm)
                       (insert-record db (assoc vc :sync-action :insert))))
                   vc)]
      (swap! currently-syncing update-in [(:tablename rec)] dissoc (:key rec))
      result)
    (catch Exception e {assoc-in rec [:errors :sync-error :exception] e})))

(defn- diff-fields [db rec current]
  (let [currentv (prep-record db current)
        diff (d/diff (:data currentv) (:data rec))]
    (when (or (first diff) (second diff))
      [(first diff) (second diff)])))

(defn diff-record
  "Take a record and diff against the target db using provided data model,
  returning record with result or errors inserted"
  [db rec]
  (let [vc (prep-record db rec)]
    (if (:prepped? vc)
                  (let [where (c/where-key-snip vc)
                        tm (m/query-record db *dm* (keyword (:tablename rec)) where)]
                    (if tm
                      (assoc vc :diff :update :data (diff-fields db vc tm))
                      (assoc vc :diff :insert)))
                  (assoc vc :diff :invalid))))

(defn- map-record
  "Prepare a record (validate and add key) then add to record map"
  [db acc pr]
    (assoc-in acc [(:tablename pr) (:key pr)] pr))

(defn- records->lookup-map
  "Create a map of prepped records by table and key"
  [db prs]
  (reset! record-lookup (reduce #(map-record db %1 %2) {} prs)))

(defn- prep-records [db rs]
  (doall (map #(prep-record db %) rs)))

(defn sync-records
  "Take a list of records and sync them to the target database using
  the target data model, returning a map containing a list of successes,
  and a list of errors."
  [db dm rs]
  (binding [*dm* dm]
           (let [prs (prep-records db rs)]
             (records->lookup-map db prs)
             (reduce
              (fn [a r]
                (let [x (sync-record db r)]
                  (if (:errors x)
                    (update a :errors conj x)
                    (update a :successes conj x))))
              {:successes [] :errors []} prs))))

(defn- run-sync
  [accum r errs]
  (-> accum
      (update :processed conj (:successes r))
      (update :runs inc)
      (assoc :last-error-count (count errs))
      (assoc :records (->> (:errors r)
                           (map #(-> %
                                     (update :data
                                             merge
                                              (get-in % [:errors :substituted]))
                                     (update :data
                                             merge
                                             (get-in % [:errors :coerce]))
                                     (dissoc :sync-action :errors)))))))

(defn run-sync-repeatedly
  "Take a list of records and repeatedly sync, accumulating a map containing
  a list of successes, a list of warnings and a list of errors. Terminates
  when the count of errors remains constant."
  [db dm rs]
  (loop [accum {:records rs
                :processed []
                :errors []
                :last-error-count nil
                :runs 0
                }]
    (let [recs (:records accum)
          r (sync-records db dm recs)
          errs (:errors r)
          _ (debug "Sync run completed, errors: " (count errs))]
      (if (and (seq r)
               (or (nil? (:last-error-count accum))
                   (> (:last-error-count accum) (count errs))))
        (recur (run-sync accum r errs))
        (-> accum
            (update :processed concat (:successes r))
            (update :runs inc)
            (dissoc :records :last-error-count)
            (assoc :errors (:errors r)))))))

(defn error-summary
  "Pull error summary info out of result error map"
  [sr]
  (map #(select-keys % [:tablename :key :identifier :errors]) (:errors sr)))

(defn delete-placeholders
  "Delete placeholder records"
  [db dm]
  (loop [r {:error [] :deleted [] :skipped []}
         le nil]
    (let [run (reduce-kv
               (fn [a k v]
                 (let [key (:key_column v)]
                   (if key
                     (try
                       (if (= 1
                              (first (jdbc/delete! db k (c/where-key-snip {:key {key -1}}))))
                         (update a :deleted conj k)
                         a)
                       (catch Exception _ (update a :error conj k)))
                     (update a :skipped conj k)))) r dm)]
      (if (and le
               (> le (count (:error r))))
        (recur run (count (:error r)))
        run))))

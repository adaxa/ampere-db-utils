FROM openjdk:11.0.8-jre-slim-buster

WORKDIR /

COPY target/uberjar/ampere-db-utils-0.1.0-standalone.jar app.jar

ENTRYPOINT ["java", "-jar", "app.jar"]

(ns ampere-db-utils.sync-test
  (:require [ampere-db-utils.core :as c]
            [ampere-db-utils.import :as i]
            [ampere-db-utils.model :as m]
            [ampere-db-utils.sync :as s]
            [clojure.java.jdbc :as jdbc]
            [clojure.test :refer :all]
            [matcher-combinators.test :refer :all]
            [aero.core :refer [read-config]]
            [ampere-db-utils.import :as i]))

(def config (read-config (clojure.java.io/resource "test-config.edn")))

(def sdb (:source-db config))
(def tdb (:target-db config))
(def sdm (atom nil))
(def tdm (atom nil))
(def test-data (atom nil))

(def ^:dynamic db nil)

(defn db-transaction-fixture [f]
  (reset! s/validated-fks #{})
  (jdbc/with-db-transaction [conn tdb]
    (jdbc/db-set-rollback-only! conn)
    (binding [db conn]
      (f))))

(defn init-test
  [test-fn]
  (reset! sdm (m/load-db-data-model sdb))
  (reset! tdm (m/load-db-data-model tdb))
  (reset! test-data (i/flatten-children (i/load-files ["dev-resources/ad_table_100.json"]) []))
  (test-fn))

(use-fixtures :once init-test)
(use-fixtures :each db-transaction-fixture)

(deftest unit-test
  (testing "Sync data from file"
    (is (= 29 (-> @test-data count)))
    ;; prepare-data adds :key 
    (is (contains? (s/prepare-data db @tdm (first @test-data)) :key))
    (is (contains? (s/update-record db @tdm (s/prepare-data db @tdm (first @test-data)) {}) :sync))
    (is (match? {:sync :success} (s/update-record db @tdm
                                                (s/prepare-data db @tdm (first @test-data)) nil)))
    ;; duplicate key error:
    (is (match? {:errors {:insert {:error {:sqlstate "23505"}}}}
                (s/insert-record db @tdm
                               (s/prepare-data db @tdm (first @test-data)))
                ))
    ;; sync
    ))

(deftest sync-
  (testing  "sync"
    (is (match? {:sync-action :update
                 :sync :success} (s/sync-record db @tdm (first @test-data))))      ))




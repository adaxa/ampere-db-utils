(ns ampere-db-utils.core-test
  (:require [ampere-db-utils.core :refer :all]
            [clojure.java.jdbc :as jdbc]
            [clojure.test :refer :all]))

(def sdb (atom nil))
(def tdb (atom nil))

(defn db-transaction-fixture [f]
  (jdbc/with-db-transaction [conn @tdb]
    (jdbc/db-set-rollback-only! conn)
    (binding [db conn]
      (f))))

(defn init-test
  [test-fn]
  (test-fn))

(use-fixtures :once init-test)
(use-fixtures :each db-transaction-fixture)



(ns ampere-db-utils.model-test
  (:require [ampere-db-utils.model :as m]
            [ampere-db-utils.core :as c]
            [clojure.java.jdbc :as jdbc]
            [clojure.java.io :as io]
            [clojure.test :refer :all]
            [aero.core :refer [read-config]]))

(def config (read-config (io/resource "test-config.edn")))

(def sdb (:source-db config))
(def dm (atom nil))

(defn clean-tmp-file
  [f]
  (f)
  (try
    (clojure.java.io/delete-file "/tmp/dm.edn")
    (catch Exception _)))

(defn init-test
  [test-fn]
  (reset! dm (m/load-db-data-model sdb))
  (test-fn))

(use-fixtures :once init-test)
(use-fixtures :each clean-tmp-file)

(deftest dm-utils
  (testing "Utility methods"
    (is (= {:ad_window_id 100}
           (:key (m/add-key @dm {:tablename :ad_window :data
                               {:ad_window_id 100 :ad_client_id 11}}))))
    (is (= {:name "Window"}
           (:identifier (m/add-identifier @dm {:tablename :ad_window :data {:ad_window_id 100 :name "Window" :ad_client_id 11}}))))
    (is (some #{"ad_org_id" "ad_table_id"} (m/cols @dm :ad_table)))))

(deftest data-model
  (testing "Loading data model from db"
    (let [_ (m/write-data-model @dm "/tmp/dm.edn")]
      (is (= :ad_table (-> @dm
                            :ad_table
                            :tablename)))
      (is (= (:ad_table @dm) (:ad_table (m/load-file-data-model "/tmp/dm.edn")))))))


(deftest coerce
  (testing "Coerce values"
    (is (= {:value nil} (m/coerce-json-value :string nil) ))
    (is (= "Value" (:value (m/coerce-json-value :string "Value")) ))
    (is (= java.time.LocalDateTime (class (:value (m/coerce-json-value :datetime "2005-11-22T11:35:48"))) ))
    (is (contains? (m/coerce-json-value :datetime "2005-11-22T11:35:48 BC") :error))
    (is (= "Failed to convert to datetime"
           (get-in (m/coerce-json-data @dm {:tablename "ad_client"
                                          :data {:created "2005-11-22T11:35:48 BC"}}) [:errors :coerce-errors :created :error])))
    ))

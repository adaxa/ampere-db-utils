(ns ampere-db-utils.export-test
  (:require [ampere-db-utils.core :as c]
            [ampere-db-utils.export :as e]
            [clojure.java.jdbc :as jdbc]
            [clojure.test :refer :all]
            [aero.core :refer [read-config]]
            [ampere-db-utils.model :as m]))

(def config (read-config (clojure.java.io/resource "test-config.edn")))

(def sdb (:source-db config))
(def dm (atom nil))

(defn clean-tmp-file
  [f]
  (f))
(try
  (clojure.java.io/delete-file "/tmp/dm.edn")
  (catch Exception _))

(defn init-test
  [test-fn]
  (reset! dm (m/load-db-data-model sdb))
  (test-fn))

(use-fixtures :once init-test)
(use-fixtures :each clean-tmp-file)


(deftest export
  (testing "Export a table"
    (is (= :ad_client (e/export-table! sdb @dm :ad_client ["ad_client_id=?" 0] "test_output/client.json") ))))


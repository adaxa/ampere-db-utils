-- :name count-table :? :1
-- :doc count :tablename matching sql :where
SELECT count(*) FROM :i:tablename
 WHERE :snip:wc ;

-- :name copy-table-to :!
-- :doc Copy table :tablename to csv file :filename
--      for client :ad_client_id with columns :columnnames
copy (select :i*:columnnames from :i:tablename
       where :snip:wc
       order by :i*:columnnames) to :sql:filename ;

-- :name copy-table-from-stdin :!
-- :doc Copy from stdin
copy :i:tablename (:i*:columnnames) from STDIN;

-- :name get-tables-to-export :? :*
-- :doc get tables from ad
select lower(tablename) as tablename, ad_table_id from ad_table
 where isview = 'N' and isactive = 'Y'
   and tablename not like 'T%'
   and tablename not like 'I%'
   and tablename not in ('AD_Attribute_Value', 'AD_PInstance_Log', 'AD_PInstance', 'AD_RecentItem', 'AD_Session')
   and (tablename = 'AD_EntityType' or not exists (select * from ad_column c where c.columnname = 'EntityType'
                                                                               and c.ad_table_id=ad_table.ad_table_id))
 order by tablename ;

-- :name get-clients :? :*
-- :doc get clients
select name, ad_client_id from ad_client
 order by ad_client_id ;

-- :name get-entitytypes :? :*
-- :doc get entitytypes
select entitytype from ad_entitytype
 order by entitytype ;

-- :name get-entity-tables :? :*
-- :doc get enitity type tables from ad
select tablename, ad_table_id from ad_table
 where isview = 'N' and isactive = 'Y'
   and tablename not like 'T%'
   and tablename not like 'I%'
   and tablename not like 'AD_EntityType'
   and exists (select * from ad_column c where c.columnname = 'EntityType' and c.ad_table_id=ad_table.ad_table_id)
 order by tablename

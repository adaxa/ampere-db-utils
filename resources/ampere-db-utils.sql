
-- :name get-table-info :? :*
-- :doc retrieve info (keys, columns, etc) for tables in JSON format
WITH spec_cols AS (
  SELECT 31 as ad_reference_id, 'm_locator'::text as tablename, 'm_locator_id'::text as columnname
   UNION
  SELECT 32 as ad_reference_id, 'ad_image'::text as tablename, 'ad_image_id'::text as columnname)
SELECT row_to_json(i, true) as info
  FROM (SELECT lower(t.tablename) as tablename, t.ad_table_id, t.name,
               t.isview,
               (SELECT array_agg(c.columnname)
                  FROM (SELECT lower(col.columnname) AS columnname
                          FROM ad_column col
                         WHERE col.ad_table_id = t.ad_table_id
                           AND isparent = 'Y'
                         ORDER BY col.SeqNo) AS c) as parent_columns,
               (SELECT lower(c.columnname)
                  FROM ad_column c
                 WHERE c.ad_table_id=t.ad_table_id
                   AND c.iskey='Y') AS key_column,
               (SELECT array_to_json(array_agg(row_to_json(c, true)), true)
                  FROM (SELECT DISTINCT ON (col.iskey, col.isidentifier,
                                            case when col.isidentifier = 'Y' then COALESCE(col.seqno, 0)
                                            else 999 end,
                                            lower(col.columnname)) lower(col.columnname) AS columnname,
                               col.iskey, col.ismandatory,
                               col.ad_reference_id, r.name,
                               lower(coalesce(t2.tablename, t3.tablename, sc.tablename)) as fk_table,
                               lower(coalesce(c2.columnname, c3.columnname, sc.columnname)) as fk_column
                          FROM ad_column col
                                 JOIN ad_reference r ON (col.ad_reference_id=r.ad_reference_id)
                                 LEFT JOIN ad_ref_table rt ON (rt.ad_reference_id=col.ad_reference_value_id)
                                 LEFT JOIN ad_table t2 ON (rt.ad_table_id=t2.ad_table_id)
                                 LEFT JOIN ad_column c2 ON (c2.ad_column_id=rt.ad_key)
                                 LEFT JOIN ad_table t3
                                     ON (t3.tablename=substring(col.columnname
                                                                for greatest(0,(length(col.columnname)-3)))
                                                                AND t3.ad_table_id <> t.ad_table_id)
                                 LEFT JOIN ad_column c3 ON (c3.ad_table_id=t3.ad_table_id and c3.iskey='Y')
                                 LEFT JOIN spec_cols sc ON (sc.ad_reference_id=col.ad_reference_id)
                         WHERE col.ad_table_id=t.ad_table_id
                           AND col.ColumnSQL IS NULL
                         ORDER BY col.iskey desc, col.isidentifier desc,
                                  case when col.isidentifier = 'Y' then COALESCE(col.seqno, 0)
                                  else 999 end, lower(col.columnname)) AS c) AS columns,
               (SELECT array_agg(c.columnname)
                  FROM (SELECT lower(col.columnname) AS columnname
                          FROM ad_column col
                         WHERE col.ad_table_id = t.ad_table_id
                           AND isidentifier = 'Y'
                         ORDER BY col.SeqNo) AS c)  AS identifiers
          FROM ad_table t
         WHERE t.isactive = 'Y' AND t.isview = 'N'
         ORDER BY t.tablename) AS i;
 
-- :snip child-json-snip
-- :doc query fragment to select data as json from child table
|| 
     (select array_agg(
       json_build_object(
         'tablename', :v:tablename,
         'data', row_to_json((select r from (select :i*:columnnames) r), true) ,
         'children', child_data))
                              as child_data
                       from (select :i*:columnnames
                                    , ARRAY[]::json[] :snip*:children as child_data
                               from :i:tablename
                              where :sql:wc
                              order by :i*:sortnames) as t) 

-- :name select-json :? :1
-- :doc select from table as json for client
select array_to_json(array_agg(json_build_object(
      'tablename', :v:tablename,
      'data', row_to_json((select r from (select :i*:columnnames) r), true),
      'children', child_data)), true) as json
               from (select :i*:columnnames
                            , ARRAY[]::json[] :snip*:children as child_data
                       from :i:tablename
                      where :snip:wc
                      order by :i*:sortnames) as t;

-- :name select-json-where :? :1
/* :require [clojure.string :as string]
   [hugsql.parameters :refer [identifier-param-quote]] */
SELECT row_to_json(i, true) as json
  FROM :i:tablename i
 WHERE :snip:wc

-- :snip where-key-snip
/* :require [clojure.string :as string]
   [hugsql.parameters :refer [identifier-param-quote]] */
/*~
  (string/join " AND "
  (for [[key-col _] (:key params)]
  (str (name key-col)
  " = " (let [v (get-in params  [:key key-col])]
  (if (string? v)
  (str "'" v "'")
  v)))))
  ~*/

-- :snip where-in
:i:columnname in (:v*:ids)
  

(defproject ampere-db-utils "0.1.0"
  :description "Utilities for working with ampere database"
  :url "http://gitlab.com/adaxa/"
  :licenses [{:name "UNLICENSE"
              :url "http://unlicense.org"}
             {:name "MIT"
              :url "http://opensource.org/licenses/MIT"}]
  :repositories {"gitlab-ampere" "https://gitlab.com/api/v4/groups/adaxa/-/packages/maven/"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [cheshire "5.8.1"]
                 [org.postgresql/postgresql "42.2.5"]
                 [clojure.java-time "0.3.2" ]
                 [org.clojure/data.csv "0.1.4"]
                 [clojurewerkz/propertied "1.3.0"]
                 [clj-time "0.14.3"]
                 [com.layerware/hugsql "0.4.9"]
                 [clojure.java-time "0.3.2"]
                 [com.taoensso/timbre "4.10.0"]
                 [com.fzakaria/slf4j-timbre "0.3.19"]
                 [commons-codec/commons-codec "1.14"]
                 [hikari-cp "2.12.0"]
                 [cli-matic "0.4.3"]
                 [nubank/matcher-combinators "3.1.1"]
                 [aero "1.1.6"]
                 [fipp "0.6.25"]]
  :repl-options {:init-ns ampere-db-utils.core}
  :main ampere-db-utils.cli
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
